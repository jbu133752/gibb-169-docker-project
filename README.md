# Reverse proxy and webserver example with docker & docker-compose
To run the example shown in this repo you have to follow the steps detailed below.

## Clone the repository
To clone the repository, you enter the following command into your commandline. Afterwards you enter newly created folder.

```
git clone https://gitlab.com/jbu133752/gibb-169-docker-project.git
cd gibb-169-docker-project
```

## build and run the example
To run the example docker compose file and build the dockerfile in reverse-proxy, you have to enter the command below.

```
docker-compose -f docker-compose-build.yml up -d
```

Running the command above, will build the dockerfile if no image with this name exists. If you already ran this command and want to build the image from scratch again, you have to enter the next couple of commands. The first one stops all containers in the docker-compose file and the second one deletes all unused images, containers and volumes.

```
docker-compose -f docker-compose-build.yml down
docker system prune -a --volumes
```

## Accessing the demo
To access the demo, you have to go to following urls in the browser:
- for the apache webserver: http://\<docker-ip\>/apache
- for the nginx webserver: http://\<docker-ip\>/nginx
